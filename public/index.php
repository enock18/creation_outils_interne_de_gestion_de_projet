<?php

    require 'connection.php';
    //selectionner tous le projets
    $requall=(" SELECT * FROM projets");
    $resultat=$conn->prepare($requall);
    $resultat->execute();
// modifier les projets
    if(isset($_GET["del_projet"])){
        $id=$_GET["del_projet"];
        $reqid=("DELETE FROM projets WHERE id=$id"); 
        $res=$conn->prepare($reqid);
        $res->execute();
        header('location:index.php');
    };

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="styles.css" rel="stylesheet">
    <title> GESTION DE PROJET</title>  
</head>
<body>
<?php
        include 'header.php'
?>

    
   
<main>

    <div class="container">
    <button class="btn-add"><a href="addprojet.php"> + Projet</a></button>
        <table>
            <thead>
                        <tr>
                            <th>PROJET</th>
                            <th class="operation"> </th>
                        </tr>

            </thead>

            
            <tbody>
                            <?php  foreach($resultat as $table){ ?>
                                <tr > 

                                    <td class="color"><?php echo $table["titre"];?></td>

                                    <td >
                                  
                                    <a href="projet.php?projet=<?php echo $table['id'] ?>"> Voir</a> |<a href="edit.php?update=<?php echo $table['id'];?>"> 
                                    Edit</a>  |  <a href="index.php?del_projet=<?php echo $table['id']; ?>">Delete</a>
                                    </td>
                                     
                                      
                                </tr> 
                            <?php }?>       
                </tbody>
        </table>
        <div class="buttons"> 
       <button class="btn-add"> <a href="addevelopper.php">+ Developpeur</a></button> 
        <button class="btn-add"> <a href="addclient.php"> + Client</a> </button>
        </div>

       
    </div>
       
</main>
    

                        
<?php

include 'footer.php'

?>
</body>
</html>