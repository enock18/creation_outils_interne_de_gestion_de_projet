-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : Dim 19 déc. 2021 à 22:05
-- Version du serveur :  8.0.27-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bd_gestion_de_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int NOT NULL,
  `firstname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `lastname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `adresse` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `firstname`, `lastname`, `adresse`) VALUES
(1, '$firstname', '$lastname', '$adresse'),
(2, 'qadad', 'adad', 'adazdad'),
(3, 'qadad', 'adad', 'adazdad'),
(4, 'qadad', 'adad', 'adazdad'),
(5, 'Enock', 'MUKOKO MAYA', '2 Rue dAquitaine'),
(6, 'Enock', 'MUKOKO MAYA', '2 Rue dAquita'),
(7, 'Enock', 'MUKOKO MAYA', '2 Rue dAquitaine'),
(8, 'Enock', 'MUKOKO MAYA', '2 Rue d&#039;Aquitaine'),
(9, 'Enock', 'MUKOKO MAYA', '2 Rue d&#039;Aquitaine');

-- --------------------------------------------------------

--
-- Structure de la table `client_projet`
--

CREATE TABLE `client_projet` (
  `fk_client` int NOT NULL,
  `fk_projet` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `developpeurs`
--

CREATE TABLE `developpeurs` (
  `id` int NOT NULL,
  `firstname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `lastname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `developpeurs`
--

INSERT INTO `developpeurs` (`id`, `firstname`, `lastname`) VALUES
(1, 'Enock', 'MUKOKO MAYA '),
(2, 'REMY ', 'Ageron'),
(3, 'Hava', 'Havaki'),
(4, 'JEREMIE', 'Dupont'),
(5, 'Enock', ''),
(6, 'Eric', 'MUKOKO MAYA '),
(7, 'Hava', 'Havaki'),
(8, 'Enock', 'MUKOKO MAYA'),
(9, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `dev_projet`
--

CREATE TABLE `dev_projet` (
  `fk_dev` int NOT NULL,
  `fk_projet` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `niveau_dev`
--

CREATE TABLE `niveau_dev` (
  `id` int NOT NULL,
  `titre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

CREATE TABLE `projets` (
  `id` int NOT NULL,
  `titre` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `projets`
--

INSERT INTO `projets` (`id`, `titre`, `description`) VALUES
(22, 'Site trinker', '  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lobortis cursus viverra. Nullam tortor metus, fringilla et condimentum a, bibendum nec magna. Ut est augue, fringilla in lacinia sed, aliquet vel odio. Praesent euismod urna vitae elit venenatis, eget finibus nisl lacinia. Quisque dapibus rutrum lorem, vitae dignissim odio blandit ut. Donec scelerisque hendrerit felis, eget scelerisque est lobortis id. Pellentesque et posuere nunc. Pellentesque placerat pulvinar lobortis. Curabitur blandit vel mi vitae facilisis. Quisque at augue vulputate, pulvinar nulla placerat, sagittis purus. Pellentesque ac magna tincidunt, rhoncus dolor vel, placerat est. Quisque sed quam a dolor pharetra porta a vel lacus.  '),
(25, 'Blog from scratch', '  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lobortis cursus viverra. Nullam tortor metus, fringilla et condimentum a, bibendum nec magna. Ut est augue, fringilla in lacinia sed, aliquet vel odio. Praesent euismod urna vitae elit venenatis, eget finibus nisl lacinia. Quisque dapibus rutrum lorem, vitae dignissim odio blandit ut. Donec scelerisque hendrerit felis, eget scelerisque est lobortis id. Pellentesque et posuere nunc. Pellentesque placerat pulvinar lobortis. Curabitur blandit vel mi vitae facilisis. Quisque at augue vulputate, pulvinar nulla placerat, sagittis purus. Pellentesque ac magna tincidunt, rhoncus dolor vel, placerat est. Quisque sed quam a dolor pharetra porta a vel lacus. '),
(26, 'Portfolio', '    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lobortis cursus viverra. Nullam tortor metus, fringilla et condimentum a, bibendum nec magna. Ut est augue, fringilla in lacinia sed, aliquet vel odio. Praesent euismod urna vitae elit venenatis, eget finibus nisl lacinia. Quisque dapibus rutrum lorem, vitae dignissim odio blandit ut. Donec scelerisque hendrerit felis, eget scelerisque est lobortis id. Pellentesque et posuere nunc. Pellentesque placerat pulvinar lobortis. Curabitur blandit vel mi vitae facilisis. Quisque at augue vulputate, pulvinar nulla placerat, sagittis purus. Pellentesque ac magna tincidunt, rhoncus dolor vel, placerat est. Quisque sed quam a dolor pharetra porta a vel lacus.  '),
(31, 'To do list', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lobortis cursus viverra. Nullam tortor metus, fringilla et condimentum a, bibendum nec magna. Ut est augue, fringilla in lacinia sed, aliquet vel odio. Praesent euismod urna vitae elit venenatis, eget finibus nisl lacinia. Quisque dapibus rutrum lorem, vitae dignissim odio blandit ut. Donec scelerisque hendrerit felis, eget scelerisque est lobortis id. Pellentesque et posuere nunc. Pellentesque placerat pulvinar lobortis. Curabitur blandit vel mi vitae facilisis. Quisque at augue vulputate, pulvinar nulla placerat, sagittis purus. Pellentesque ac magna tincidunt, rhoncus dolor vel, placerat est. Quisque sed quam a dolor pharetra porta a vel lacus.');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client_projet`
--
ALTER TABLE `client_projet`
  ADD PRIMARY KEY (`fk_client`,`fk_projet`);

--
-- Index pour la table `developpeurs`
--
ALTER TABLE `developpeurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `dev_projet`
--
ALTER TABLE `dev_projet`
  ADD PRIMARY KEY (`fk_dev`,`fk_projet`);

--
-- Index pour la table `niveau_dev`
--
ALTER TABLE `niveau_dev`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `projets`
--
ALTER TABLE `projets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `developpeurs`
--
ALTER TABLE `developpeurs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `niveau_dev`
--
ALTER TABLE `niveau_dev`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `projets`
--
ALTER TABLE `projets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
